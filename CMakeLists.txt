cmake_minimum_required(VERSION 2.8)
include(CheckCSourceCompiles)
find_package(PkgConfig)
pkg_check_modules(LWS REQUIRED libwebsockets)

set(SAMP demo-websocket) 
set(SRCS demo-websocket.c)

add_executable(${SAMP} ${SRCS})
target_link_libraries(${SAMP} ${LWS_LIBRARIES})
target_include_directories(${SAMP} PUBLIC ${LWS_INCLUDE_DIRS})
target_compile_options(${SAMP} PUBLIC ${LWS_CFLAGS_OTHER})

install(TARGETS ${SAMP} RUNTIME DESTINATION bin)
install(DIRECTORY html DESTINATION /var/www/websocket)
install(PROGRAMS ${SAMP}.init DESTINATION /etc/init.d RENAME ${SAMP})
